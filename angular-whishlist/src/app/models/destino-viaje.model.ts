import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    private selected: boolean;
    public servicios: String[];
    id = uuid();

    constructor(public nombre: string, public imagenUrl: string, public votes: number = 0) {
        this.servicios = ['desayuno', 'cena'];
     }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }
}
